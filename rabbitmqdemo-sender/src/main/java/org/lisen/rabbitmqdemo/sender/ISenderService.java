package org.lisen.rabbitmqdemo.sender;

public interface ISenderService {

    void sendDirectMessage(String message);

    void sendDirectMessage2(String message);

    void sendUser();

    void sendTopicMsg();

    void sendFanoutMsg();

    void sendUsualMsg();
}
