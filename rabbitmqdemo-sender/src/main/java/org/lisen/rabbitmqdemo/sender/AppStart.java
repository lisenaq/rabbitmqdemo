package org.lisen.rabbitmqdemo.sender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppStart {

    public static void main(String[] arg) {
        SpringApplication.run(AppStart.class, arg);
    }

}
