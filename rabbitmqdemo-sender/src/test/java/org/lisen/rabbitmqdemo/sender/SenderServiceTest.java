package org.lisen.rabbitmqdemo.sender;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=AppStart.class)
@EnableAutoConfiguration
public class SenderServiceTest {

    @Resource
    private ISenderService senderService;

    /**
     * Direct简单队列，发送一个字符串消息
     */
    @Test
    public void testSendMessage() {
        senderService.sendDirectMessage("hello rabbit mq 1... ");
    }

    /**
     * Direct简单队列，发送User对象，该对象将被转换为json并
     * 发送到队列中去
     */
    @Test
    public void testSendUsers() {
        senderService.sendUser();
    }

    /**
     * 主题订阅模式，消息将被交换机转发到所有关心Routing-key指定的队列中
     */
    @Test
    public void testSendTopicMsg() {
        senderService.sendTopicMsg();
    }

    /**
     * 广播模式，消息将被转发到所有与交换机绑定的队列中
     */
    @Test
    public void testSendFanoutMsg() {
        senderService.sendFanoutMsg();
    }

    /**
     * 死信队列实例，发送的信息在超时之后将被转发到死信队列
     */
    @Test
    public void testSendWillDXLsg() {
        senderService.sendUsualMsg();
    }

}
