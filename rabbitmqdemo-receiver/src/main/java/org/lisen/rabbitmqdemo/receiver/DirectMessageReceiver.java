package org.lisen.rabbitmqdemo.receiver;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class DirectMessageReceiver {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RabbitListener(queues=QueueConfig.QUEUE_DIRECT)
    public void message(Channel channel, Message message) throws IOException {

        /*
         * 此处可以放置业务处理代码，如果业务处理出现异常，则可根据异常是否是
         * 可恢复的，如果是可恢复异常（如网络抖动引发的异常）则可以让信息重新
         * 入队以便于再次处理，如果是不可恢复的异常则可以记录异常日志以便于处
         * 理
         */

        /*
         * deliveryTag:该消息的index
         * multiple：是否批量.true:将一次性ack所有小于deliveryTag的消息。
         */
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);

        /*
         * deliveryTag:该消息的index
         * multiple：是否批量.true:将一次性拒绝所有小于deliveryTag的消息。
         * requeue：被拒绝的是否重新入队列，
         */
        //channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
        logger.info("R1：DIRECT:: " + new String(message.getBody()));
    }

}
