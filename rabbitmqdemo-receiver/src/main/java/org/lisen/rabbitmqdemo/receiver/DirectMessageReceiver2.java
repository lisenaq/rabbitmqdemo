package org.lisen.rabbitmqdemo.receiver;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

//@Component
public class DirectMessageReceiver2 {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RabbitListener(queues=QueueConfig.QUEUE_DIRECT)
    public void message(Message message, Channel channel) throws IOException {
        //channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
        channel.basicNack(message.getMessageProperties().getDeliveryTag(), false,true);
        logger.info("R2：DIRECT:: " + new String(message.getBody()));
    }

}
